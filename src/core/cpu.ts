/**
 * 專案名稱： gof-builder
 * 檔案說明： 抽象 CPU
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 抽象 CPU
 */
export interface CPU {
  /**
   * 廠牌
   */
  brand: string;
  /**
   * 型號
   */
  model: string;
}
