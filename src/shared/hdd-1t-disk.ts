/**
 * 專案名稱： gof-builder
 * 檔案說明： 1T HDD 硬碟
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Disk } from './../core';

/**
 * 1T HDD 硬碟
 */
export class HDD1TDisk implements Disk {
  /**
   * 硬碟類型
   */
  public type: 'HDD' | 'SSD' = 'HDD';
  /**
   * 硬碟大小
   */
  public size = '1T';
}
