/**
 * 專案名稱： gof-builder
 * 檔案說明： 專案統合資料建構者
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { SummaryBuilder } from '../core';
import { Member, Summary } from '../models';

/**
 * 專案統合資料建構者
 */
export class ProjectSummaryBuilder extends SummaryBuilder<Summary> {
  /**
   * 取得成員資訊
   *
   * @method private
   * @param member 成員資料
   * @return 回傳成員資訊
   */
  private pickMemberInfo(member: Member): Omit<Member, 'role'> {
    return {
      userId: member.userId,
      username: member.username,
    };
  }

  /**
   * 建構統合後的專案資料
   *
   * @method public
   * @return 回傳統合後的專案資料
   */
  public build(): Summary {
    const project = this.project;
    const members = this.members;
    const summary: Summary = {
      id: project.id,
      mode: project.mode,
      projectName: project.projectName,
      launchDate: project.launchDate,
      dueDate: project.dueDate,
      member: {},
    };

    if (project.mode === 'scrum') {
      members.forEach(member => {
        if (member.role === 'scrum-master') {
          summary.member.scrumMaster = this.pickMemberInfo(member);
        } else if (member.role === 'product-owner') {
          summary.member.productOwner = this.pickMemberInfo(member);
        } else if (member.role === 'developer') {
          summary.member.developer = this.pickMemberInfo(member);
        }
      });
    } else if (project.mode === 'waterfall') {
      members.forEach(member => {
        if (member.role === 'system-pic') {
          summary.member.systemPic = this.pickMemberInfo(member);
        } else if (member.role === 'it-pic') {
          summary.member.itPic = this.pickMemberInfo(member);
        } else if (member.role === 'developer') {
          summary.member.developer = this.pickMemberInfo(member);
        }
      });
    }

    return summary;
  }
}
