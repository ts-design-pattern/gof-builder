/**
 * 專案名稱： gof-builder
 * 檔案說明： 抽象電腦
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 抽象電腦
 */
export interface Computer {
  /**
   * CPU 型號
   */
  cpu: string;
  /**
   * 記憶體廠牌
   */
  memory: string;
  /**
   * 記憶體大小
   */
  memorySize: string;
  /**
   * 硬碟類型
   */
  diskType: 'HDD' | 'SSD';
  /**
   * 硬碟大小
   */
  diskSize: string;
}
