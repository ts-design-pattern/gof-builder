/**
 * 專案名稱： gof-builder
 * 檔案說明： 專案資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 專案資料模型
 */
export interface Project {
  /**
   * 專案流水號
   */
  id: number;
  /**
   * 開發模式
   */
  mode: 'scrum' | 'waterfall';
  /**
   * 專案名稱
   */
  projectName: string;
  /**
   * 專案開始時間
   */
  launchDate: number;
  /**
   * 專案截止時間
   */
  dueDate: number;
}
