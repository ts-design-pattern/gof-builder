/**
 * 專案名稱： gof-builder
 * 檔案說明： GSkill 16G 記憶體
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Memory } from './../core';

/**
 * GSkill 16G 記憶體
 */
export class GSkill16GMemory implements Memory {
  /**
   * 記憶體廠牌
   */
  public brand = 'G.Skill';
  /**
   * 記憶體大小
   */
  public size = '16G';
}
