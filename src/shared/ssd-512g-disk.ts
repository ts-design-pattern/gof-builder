/**
 * 專案名稱： gof-builder
 * 檔案說明： 512G SSD 固態硬碟
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Disk } from '../core';

/**
 * 512G SSD 固態硬碟
 */
export class SSD512GDisk implements Disk {
  /**
   * 硬碟類型
   */
  public type: 'HDD' | 'SSD' = 'SSD';
  /**
   * 硬碟大小
   */
  public size = '512G';
}
