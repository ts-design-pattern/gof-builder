/**
 * 專案名稱： gof-builder
 * 檔案說明： 專案成員資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { MemeberType } from './member.type';

/**
 * 專案成員資料模型
 */
export interface Member {
  /**
   * 使用者工號
   */
  userId: string;
  /**
   * 使用者名稱
   */
  username: string;
  /**
   * 使用者角色
   */
  role: MemeberType;
}
