/**
 * 專案名稱： gof-builder
 * 檔案說明： 電腦販售商家
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Computer, ComputerBuilder } from './../core';

/**
 * 電腦販售商家
 */
export class ComputerShop extends ComputerBuilder {
  /**
   * 組合出電腦
   *
   * @method public
   * @return 回傳電腦
   */
  public build(): Computer {
    return {
      cpu: `${this.cpu.brand} - ${this.cpu.model}`,
      memory: this.memory.brand,
      memorySize: this.memory.size,
      diskType: this.disk.type,
      diskSize: this.disk.size,
    };
  }
}
