/**
 * 專案名稱： gof-builder
 * 檔案說明： 建構者範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import {
  ComputerShop,
  GSkill32GMemory,
  IntelI79700kCPU,
  SSD512GDisk,
} from './shared';

const cpu = new IntelI79700kCPU();
const memory = new GSkill32GMemory();
const disk = new SSD512GDisk();
const computer = new ComputerShop()
  .setCPU(cpu)
  .setMemory(memory)
  .setDisk(disk)
  .build();
console.log(computer);
