/**
 * 專案名稱： gof-builder
 * 檔案說明： 抽象專案統合資料建構者
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Member, Project } from '../models';

/**
 * 抽象專案統合資料建構者
 */
export abstract class SummaryBuilder<T> {
  /**
   * 專案資料
   */
  private _project?: Project;
  /**
   * 專案成員資料
   */
  private _members: Member[] = [];

  /**
   * 取得專案資料
   *
   * @method public
   * @return 回傳專案資料
   */
  public get project(): Project {
    if (this._project) {
      return this._project;
    } else {
      throw new Error('Missing project');
    }
  }

  /**
   * 添加專案資料
   *
   * @method public
   * @param project 專案資料
   * @return 回傳物件本身
   */
  public addProject(project: Project): this {
    this._project = project;
    return this;
  }

  /**
   * 取得專案成員資料
   *
   * @method public
   * @return 回傳專案成員資料
   */
  public get members(): Member[] {
    return this._members;
  }

  /**
   * 添加專案成員資料
   *
   * @method public
   * @param members 專案成員資料
   * @return 回傳物件本身
   */
  public addMembers(members: Member[]): this {
    this._members = members;
    return this;
  }

  /**
   * 建構統合後的專案資料
   *
   * @method public
   * @return 回傳統合後的專案資料
   */
  public abstract build(): T;
}
