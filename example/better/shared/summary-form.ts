/**
 * 專案名稱： gof-builder
 * 檔案說明： 統合表單
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Summary } from './../models';

/**
 * 統合表單
 */
export class SummaryForm {
  /**
   * 將表單提交
   *
   * @method public
   */
  public submit(summary: Summary): void {
    console.log('Send project summary data', summary);
  }
}
