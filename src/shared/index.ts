/**
 * 專案名稱： gof-builder
 * 檔案說明： 共享功能匯出點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

export * from './amd-ryzen-5900-cpu';
export * from './computer-shop';
export * from './gskill-16g-memory';
export * from './gskill-32g-memory';
export * from './hdd-1t-disk';
export * from './intel-i7-9700k-cpu';
export * from './ssd-512g-disk';
