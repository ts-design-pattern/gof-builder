/**
 * 專案名稱： gof-builder
 * 檔案說明： 專案成員類型
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 專案成員類型
 */
export type MemeberType =
  | 'scrum-master'
  | 'product-owner'
  | 'system-pic'
  | 'it-pic'
  | 'developer';
