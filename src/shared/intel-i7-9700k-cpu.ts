/**
 * 專案名稱： gof-builder
 * 檔案說明： Intel I7 9700K CPU
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { CPU } from './../core';

/**
 * Intel I7 9700K CPU
 */
export class IntelI79700kCPU implements CPU {
  /**
   * 廠牌
   */
  public brand = 'Intel';
  /**
   * 型號
   */
  public model = 'I7 9700K';
}
