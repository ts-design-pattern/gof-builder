/**
 * 專案名稱： gof-builder
 * 檔案說明： 較佳的開發模式程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import * as MEMBERS from './../../mock/data/members.json';
import * as PROJECT from './../../mock/data/project.json';
import { Member, Project } from './models';
import { ProjectSummaryBuilder, SummaryForm } from './shared';

const summary = new ProjectSummaryBuilder()
  .addProject(PROJECT as Project)
  .addMembers(MEMBERS as Member[])
  .build();
const form = new SummaryForm();
form.submit(summary);
