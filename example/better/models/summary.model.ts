/**
 * 專案名稱： gof-builder
 * 檔案說明： 專案統合資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Member } from '.';

/**
 * 專案統合資料模型
 */
export interface Summary {
  /**
   * 專案流水號
   */
  id: number;
  /**
   * 開發模式
   */
  mode: 'scrum' | 'waterfall';
  /**
   * 專案名稱
   */
  projectName: string;
  /**
   * 專案開始時間
   */
  launchDate: number;
  /**
   * 專案截止時間
   */
  dueDate: number;
  /**
   * 成員資料
   */
  member: {
    /**
     * Scrum Master 成員資料
     */
    scrumMaster?: Omit<Member, 'role'>;
    /**
     * Product Owner 成員資料
     */
    productOwner?: Omit<Member, 'role'>;
    /**
     * 系統 PIC 成員資料
     */
    systemPic?: Omit<Member, 'role'>;
    /**
     * IT PIC 成員資料
     */
    itPic?: Omit<Member, 'role'>;
    /**
     * 開發人員成員資料
     */
    developer?: Omit<Member, 'role'>;
  };
}
