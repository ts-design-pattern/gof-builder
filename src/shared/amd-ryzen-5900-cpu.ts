/**
 * 專案名稱： gof-builder
 * 檔案說明： AMD Ryzen 5900 CPU
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { CPU } from './../core';

/**
 * AMD Ryzen 5900 CPU
 */
export class AMDRyzen5900CPU implements CPU {
  /**
   * 廠牌
   */
  public brand = 'AMD';
  /**
   * 型號
   */
  public model = 'Ryzen 5900';
}
