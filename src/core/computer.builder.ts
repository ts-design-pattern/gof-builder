/**
 * 專案名稱： gof-builder
 * 檔案說明： 抽象電腦建構者
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Computer } from './computer';
import { CPU } from './cpu';
import { Disk } from './disk';
import { Memory } from './memory';

/**
 * 抽象電腦建構者
 */
export abstract class ComputerBuilder {
  /**
   * CPU
   */
  private _cpu?: CPU;
  /**
   * 記憶體
   */
  private _memory?: Memory;
  /**
   * 硬碟
   */
  private _disk?: Disk;

  /**
   * 取得 CPU
   *
   * @method public
   * @return 回傳 CPU
   */
  public get cpu(): CPU {
    if (this._cpu) {
      return this._cpu;
    } else {
      throw new Error('Missing CPU');
    }
  }

  /**
   * 安裝 CPU
   *
   * @method public
   * @param cpu CPU
   * @return 回傳物件本身
   */
  public setCPU(cpu: CPU): this {
    this._cpu = cpu;
    return this;
  }

  /**
   * 取得記憶體
   *
   * @method public
   * @return 回傳記憶體
   */
  public get memory(): Memory {
    if (this._memory) {
      return this._memory;
    } else {
      throw new Error('Missing memory');
    }
  }

  /**
   * 安裝記憶體
   *
   * @method public
   * @param memory 記憶體
   * @return 回傳物件本身
   */
  public setMemory(memory: Memory): this {
    this._memory = memory;
    return this;
  }

  /**
   * 取得硬碟
   *
   * @method public
   * @return 回傳硬碟
   */
  public get disk(): Disk {
    if (this._disk) {
      return this._disk;
    } else {
      throw new Error('Missing disk');
    }
  }

  /**
   * 安裝硬碟
   *
   * @method public
   * @param disk 硬碟
   * @return 回傳物件本身
   */
  public setDisk(disk: Disk): this {
    this._disk = disk;
    return this;
  }

  /**
   * 組合出電腦
   *
   * @method public
   * @return 回傳電腦
   */
  public abstract build(): Computer;
}
